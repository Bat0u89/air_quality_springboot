package com.osotnikov.deptassigment.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.osotnikov.deptassignment.rest.api_and_service.mapper.ApplicationRestApiToServiceModelMapper;
import com.osotnikov.deptassignment.rest.model.City;
import com.osotnikov.deptassignment.rest.model.Country;
import com.osotnikov.deptassignment.rest.model.MeasurementGroup;
import com.osotnikov.deptassignment.service.AirQualityInformationService;

@RestController
public class AirQualityInformationRestController {

	@Autowired
	private AirQualityInformationService airQualityInformationService;

	@Autowired
	ApplicationRestApiToServiceModelMapper applicationRestApiToServiceModelMapper;
	
	@GetMapping("/rest/country")
	public List<Country> getAllAvailableCountries() {

		List<com.osotnikov.deptassignment.model.Country> countries = airQualityInformationService.getAllSupportedCountries();

		return applicationRestApiToServiceModelMapper.mapCountriesToRest(countries);
	}
	
	@GetMapping("/rest/city")
	public List<City> getCities(@RequestParam(value="country_code") String countryCode) {

		List<com.osotnikov.deptassignment.model.City> cities = airQualityInformationService.getCities(countryCode);

		return applicationRestApiToServiceModelMapper.mapCitiesToRest(cities);
	}
	
	@GetMapping("/rest/measurement")
	public List<MeasurementGroup> getMeasurements(@RequestParam(value="country_code") String countryCode, @RequestParam(value="city") String city) {

		List<com.osotnikov.deptassignment.model.MeasurementGroup> measurementGroups = airQualityInformationService.getMeasurementGroups(countryCode, city);

		return applicationRestApiToServiceModelMapper.mapMeasurementGroupsToRest(measurementGroups);
	}

}
