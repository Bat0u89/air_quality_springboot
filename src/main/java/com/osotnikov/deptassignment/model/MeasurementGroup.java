package com.osotnikov.deptassignment.model;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MeasurementGroup {

	private String location;
	private double distance; 
	
	private List<Measurement> measurements;
	private Coordinates coordinates;
}
