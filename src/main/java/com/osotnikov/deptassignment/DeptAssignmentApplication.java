package com.osotnikov.deptassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.osotnikov")
public class DeptAssignmentApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(DeptAssignmentApplication.class, args);
	}

}
