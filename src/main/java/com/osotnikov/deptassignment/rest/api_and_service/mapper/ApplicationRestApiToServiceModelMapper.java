package com.osotnikov.deptassignment.rest.api_and_service.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.osotnikov.deptassignment.model.City;
import com.osotnikov.deptassignment.model.Country;
import com.osotnikov.deptassignment.model.MeasurementGroup;

@Service
public class ApplicationRestApiToServiceModelMapper {

	private ModelMapper modelMapper = new ModelMapper();

	public com.osotnikov.deptassignment.rest.model.Country mapToRest(Country country) {
		 
		return modelMapper.map(country, com.osotnikov.deptassignment.rest.model.Country.class);
	}
	
	public Country map(com.osotnikov.deptassignment.rest.model.Country restCountry) {
 
		return modelMapper.map(restCountry, Country.class);
	}
	
	public List<com.osotnikov.deptassignment.rest.model.Country> mapCountriesToRest(List<Country> countries) {
		 
		return countries.parallelStream().map(this::mapToRest).collect(Collectors.toList());
	}

	public com.osotnikov.deptassignment.rest.model.City mapToRest(City city) {
		 
		return modelMapper.map(city, com.osotnikov.deptassignment.rest.model.City.class);
	}
	
	public City map(com.osotnikov.deptassignment.rest.model.City restCountry) {
 
		return modelMapper.map(restCountry, City.class);
	}
	
	public List<com.osotnikov.deptassignment.rest.model.City> mapCitiesToRest(List<City> cities) {
		 
		return cities.parallelStream().map(this::mapToRest).collect(Collectors.toList());
	}
	
	public com.osotnikov.deptassignment.rest.model.MeasurementGroup map(MeasurementGroup measurmentGroup) {
		 
		return modelMapper.map(measurmentGroup, com.osotnikov.deptassignment.rest.model.MeasurementGroup.class);
	}
	
	public List<com.osotnikov.deptassignment.rest.model.MeasurementGroup> mapMeasurementGroupsToRest(List<MeasurementGroup> measurementGroups) {
		 
		return measurementGroups.parallelStream().map(this::map).collect(Collectors.toList());
	}

}
