package com.osotnikov.deptassignment.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Country {

	private String name;
	private String code;

}
