package com.osotnikov.deptassignment.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AveragingPeriod {

	private String unit;
	private float value;

}
