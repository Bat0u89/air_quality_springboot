package com.osotnikov.deptassignment.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Coordinates {

	private float latitude;
	private float longitude;
}
