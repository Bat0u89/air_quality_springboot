package com.osotnikov.deptassignment.rest.model;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MeasurementGroup {

	private String location;
	private String city;
	private String country;
	private double distance; 
	
	private List<Measurement> measurements;
	private Coordinates coordinates;
}
