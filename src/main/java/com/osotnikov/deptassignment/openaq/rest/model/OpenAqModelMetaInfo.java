package com.osotnikov.deptassignment.openaq.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OpenAqModelMetaInfo {

	private String name;
	private String license;
	private String website;
	private long page;
	private long limit;
	private long found;

}
