package com.osotnikov.deptassignment.openaq.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CityResponse {

	private OpenAqModelMetaInfo meta;

	private City[] results;
}
