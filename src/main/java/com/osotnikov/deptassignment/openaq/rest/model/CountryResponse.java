package com.osotnikov.deptassignment.openaq.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CountryResponse {

	private OpenAqModelMetaInfo meta;

	private Country[] results;

}
