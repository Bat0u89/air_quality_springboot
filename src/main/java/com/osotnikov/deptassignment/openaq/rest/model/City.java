package com.osotnikov.deptassignment.openaq.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class City {

	private String city;
	private String country;
	private String locations;
	private String count;
}
