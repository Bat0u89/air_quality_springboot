package com.osotnikov.deptassignment.openaq.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.osotnikov.web.http.HttpUtilities;

@Configuration
public class RestClientConfiguration {

	private HttpUtilities httpUtils = new HttpUtilities();
	private RestTemplate restTemplate = new RestTemplate();

	@Bean
	public HttpUtilities getHttpUtilities() {
		return httpUtils;
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

}
