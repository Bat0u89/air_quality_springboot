package com.osotnikov.deptassignment.openaq.rest.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CityRequest {

	private String country;
	private String orderBy;
	private String sort;
	@Builder.Default
	private int limit = Integer.MAX_VALUE;
	@Builder.Default
	private int page = 1;
}
