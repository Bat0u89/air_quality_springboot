package com.osotnikov.deptassignment.openaq.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Country {

	private String name;
	private String code;
	private long cities;
	private long locations;
	private long count;
}
