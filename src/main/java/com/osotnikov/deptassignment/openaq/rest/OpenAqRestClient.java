package com.osotnikov.deptassignment.openaq.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.osotnikov.deptassignment.openaq.rest.model.CityRequest;
import com.osotnikov.deptassignment.openaq.rest.model.CityResponse;
import com.osotnikov.deptassignment.openaq.rest.model.CountryRequest;
import com.osotnikov.deptassignment.openaq.rest.model.CountryResponse;
import com.osotnikov.deptassignment.openaq.rest.model.MeasurementRequest;
import com.osotnikov.deptassignment.openaq.rest.model.MeasurementResponse;
import com.osotnikov.web.http.HttpUtilities;
import com.osotnikov.web.rest.client.RestClientBase;

@Component
public class OpenAqRestClient extends RestClientBase {

	private static final String BASE_URL = "https://api.openaq.org/v1";
	private static final String CITIES_PATH = "/cities";
	private static final String COUNTRIES_PATH = "/countries";
	private static final String MEASUREMENTS_PATH = "/latest";

	@Autowired
	protected OpenAqRestClient(HttpUtilities httpUtils, RestTemplate restTemplate) {

		super(BASE_URL, httpUtils, restTemplate);
	}

	public CountryResponse getCountries(CountryRequest countryRequest) {

		return getResponseForRequestAsQueryParams(countryRequest, COUNTRIES_PATH, CountryResponse.class);
	}

	public CityResponse getCities(CityRequest countryRequest) {

		return getResponseForRequestAsQueryParams(countryRequest, CITIES_PATH, CityResponse.class);
	}
	
	public MeasurementResponse getMeasurements(MeasurementRequest measurementRequest) {

		return getResponseForRequestAsQueryParams(measurementRequest, MEASUREMENTS_PATH, MeasurementResponse.class);
	}

}
