package com.osotnikov.deptassignment.openaq.rest.model;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MeasurementResponse {

	private OpenAqModelMetaInfo meta;

	private List<MeasurementGroup> results;
	
}
