package com.osotnikov.deptassignment.openaq.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Measurement {

	private String parameter;
	private float value;
	private String lastUpdated;
	private String unit;
	private String sourceName;
	private AveragingPeriod averagingPeriod;

}
