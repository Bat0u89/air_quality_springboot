package com.osotnikov.deptassignment.openaq_and_service.mapper;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import com.osotnikov.deptassignment.model.City;
import com.osotnikov.deptassignment.model.Country;
import com.osotnikov.deptassignment.model.MeasurementGroup;
import com.osotnikov.deptassignment.openaq.rest.model.CityResponse;
import com.osotnikov.deptassignment.openaq.rest.model.MeasurementResponse;

@Service
public class OpenaqRestToServiceModelMapper {

	private ModelMapper modelMapper = new ModelMapper();

	@PostConstruct
	private void init() {

		initCityExplicitMapping();
	}
	
	private void initCityExplicitMapping() {
		
		TypeMap<com.osotnikov.deptassignment.openaq.rest.model.City, City> typeMap = 
			modelMapper.createTypeMap(com.osotnikov.deptassignment.openaq.rest.model.City.class, City.class);

		typeMap.addMappings(mapper -> {
			mapper.map(src -> src.getCity(), City::setName);
		});
	}

	public Country map(com.osotnikov.deptassignment.openaq.rest.model.Country restCountry) {

		Country country = modelMapper.map(restCountry, Country.class);
		return country;
	}

	public List<Country> map(com.osotnikov.deptassignment.openaq.rest.model.CountryResponse countryResponse) {

		com.osotnikov.deptassignment.openaq.rest.model.Country[] countriesRestModel = countryResponse.getResults();

		Type targetListType = new TypeToken<List<Country>>(){}.getType();
		List<Country> countries = modelMapper.map(countriesRestModel, targetListType);

		return countries;
	}

	public City map(com.osotnikov.deptassignment.openaq.rest.model.City restCity) {

		return modelMapper.map(restCity, City.class);
	}

	public List<City> map(CityResponse cityResponse) {

		com.osotnikov.deptassignment.openaq.rest.model.City[] citiesRestModel = cityResponse.getResults();
		List<City> cities = Arrays.asList(citiesRestModel).parallelStream().map(this::map).collect(Collectors.toList());

		return cities;

	}
	
	public MeasurementGroup map(com.osotnikov.deptassignment.openaq.rest.model.MeasurementGroup measurementGroup) {
		
		return modelMapper.map(measurementGroup, MeasurementGroup.class);
	}
	
	public List<MeasurementGroup> map(MeasurementResponse measureResponse) {

		List<com.osotnikov.deptassignment.openaq.rest.model.MeasurementGroup> measureGroupsRestModel = measureResponse.getResults();
		List<MeasurementGroup> measurementGroups = measureGroupsRestModel.parallelStream().map(this::map).collect(Collectors.toList());

		return measurementGroups;

	}

}
