package com.osotnikov.deptassignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osotnikov.deptassignment.model.City;
import com.osotnikov.deptassignment.model.Country;
import com.osotnikov.deptassignment.model.MeasurementGroup;
import com.osotnikov.deptassignment.openaq.rest.OpenAqRestClient;
import com.osotnikov.deptassignment.openaq.rest.model.CityRequest;
import com.osotnikov.deptassignment.openaq.rest.model.CityResponse;
import com.osotnikov.deptassignment.openaq.rest.model.CountryRequest;
import com.osotnikov.deptassignment.openaq.rest.model.CountryResponse;
import com.osotnikov.deptassignment.openaq.rest.model.MeasurementRequest;
import com.osotnikov.deptassignment.openaq.rest.model.MeasurementResponse;
import com.osotnikov.deptassignment.openaq_and_service.mapper.OpenaqRestToServiceModelMapper;

@Service
public class AirQualityInformationService {

	@Autowired
	private OpenAqRestClient openAqRestClient;

	@Autowired
	private OpenaqRestToServiceModelMapper restToServiceModelMapper;

	public List<Country> getAllSupportedCountries() {

		CountryRequest countryRequest = CountryRequest.builder().limit(Integer.MAX_VALUE).sort("asc").build();
		CountryResponse countryResponse = openAqRestClient.getCountries(countryRequest);

		return restToServiceModelMapper.map(countryResponse);
	}

	public List<City> getCities(Country country) {

		CityRequest cityRequest = CityRequest.builder().country(country.getCode()).limit(Integer.MAX_VALUE).sort("asc").build();
		CityResponse cityResponse = openAqRestClient.getCities(cityRequest);

		return restToServiceModelMapper.map(cityResponse);
	}
	
	public List<City> getCities(String countryCode) {

		CityRequest cityRequest = CityRequest.builder().country(countryCode).limit(Integer.MAX_VALUE).sort("asc").build();
		CityResponse cityResponse = openAqRestClient.getCities(cityRequest);

		return restToServiceModelMapper.map(cityResponse);
	}
	
	public List<MeasurementGroup> getMeasurementGroups(String countryCode, String city) {
		
		MeasurementRequest measurementRequest = MeasurementRequest.builder().country(countryCode).city(city).build();
		MeasurementResponse measurementResponse = openAqRestClient.getMeasurements(measurementRequest);
		
		return restToServiceModelMapper.map(measurementResponse);
	}

}
