package com.osotnikov.web.http;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Map;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HttpUtilities {

	public URI createUriWithQueryParams(String baseUrl, String urlPath, Object queryParametersAsObject) {

		// object to Map
	    ObjectMapper objectMapper = new ObjectMapper();
	    Map<String, String> map = objectMapper.convertValue(queryParametersAsObject, new TypeReference<Map<String,String>>() {});

	    // Map to MultiValueMap
	    LinkedMultiValueMap<String, String> linkedMultiValueMap = new LinkedMultiValueMap<>();
	    map.entrySet().forEach(
	    	param -> {
	    		if(param.getValue() != null) { // skip params that are null
	    			try {
						linkedMultiValueMap.add(param.getKey(), URLEncoder.encode(param.getValue(),"UTF-8" ));
					} catch (UnsupportedEncodingException e) {
						log.error(e.getMessage(), e);
					}
	    		}
	    	}
	    );

	    return UriComponentsBuilder.fromUriString(baseUrl).path(urlPath).queryParams(linkedMultiValueMap).build().toUri();

	}

}