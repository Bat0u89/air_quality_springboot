package com.osotnikov.web.rest.client;

import java.net.URI;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.osotnikov.web.http.HttpUtilities;

public class RestClientBase {

	private String baseUrl;
	private HttpUtilities httpUtils;
	private RestTemplate restTemplate;

	protected RestClientBase(String baseUrl, HttpUtilities httpUtils, RestTemplate restTemplate) {
		super();
		this.baseUrl = baseUrl;
		this.httpUtils = httpUtils;
		this.restTemplate = restTemplate;
	}

	public <R, T> T getResponseForRequestAsQueryParams(R request, String urlPath, Class<T> responseClass) {

		URI uri = httpUtils.createUriWithQueryParams(baseUrl, urlPath, request);

		ResponseEntity<T> response = restTemplate.exchange(
			uri.toString(),
			HttpMethod.GET,
			null,
			responseClass);

		return response.getBody();
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public HttpUtilities getHttpUtils() {
		return httpUtils;
	}

	public void setHttpUtils(HttpUtilities httpUtils) {
		this.httpUtils = httpUtils;
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

}
