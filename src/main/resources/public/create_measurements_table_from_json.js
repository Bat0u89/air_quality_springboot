function createMeasurementsTableFromJson(measurementGroups) {

    var transformMeasurements =
    {
        '<>': 'tr', 'html': [
            { '<>': 'td', 'text': '${parameter}' },
            { '<>': 'td', 'text': '${value}' },
            { '<>': 'td', 'text': '${unit}' },
            { '<>': 'td', 'text': '${lastUpdated}' },
            { '<>': 'td', 'text': '${sourceName}' },
            { '<>': 'td', 'text': '${averagingPeriod.value} ${averagingPeriod.unit}' }
        ]
    };

    var transformMeasurementGroups = [
        {
            '<>': 'tbody', 'class': 'labels', 'html': [
                {
                    '<>': 'tr', 'html': [
                        {
                            '<>': 'td', 'colspan': 6, 'html': [
                                { '<>': 'label', 'for': '${location}', 'text': '${location}' },
                                { '<>': 'input', 'type': 'checkbox', 'name': '${location}', 'id': '${location}', 'data-toggle': 'toggle', 'html': ['${location}'] }
                            ]
                        }
                    ]
                }
            ]
        },

        {
            '<>': 'tbody', 'class': 'hide measurement', 'html':
                function () {
                    return json2html.transform(this.measurements, transformMeasurements);
                }
        }

    ];

    var measurementGroupsHtml = json2html.transform(measurementGroups, transformMeasurementGroups);

    $('.labels').remove();
    $('.measurement').remove();
    $('#table_header_for_measurement_results').after(measurementGroupsHtml);

    // make measure groups toggleable

    $('[data-toggle="toggle"]').change(function () {
        $(this).parents().next('.hide').toggle();
    });
};