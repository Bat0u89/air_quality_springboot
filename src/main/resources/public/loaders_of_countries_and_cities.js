$(document).ready(function(){
	
	var $countrySelector = $('#countrySelectId');
	
	// load up available countries on startup
	(function loadCountries() {
		
    	  $countrySelector.append('<option></option>');
    	  
    	  $.ajax({
    	     	url: $countrySelector.attr('data-source'),
    	  }).then(function(options) {
    		  
	    	    options.map(function(option) {
		    	      var $option = $('<option>');
		    	      
		    	      $option
		    	        .val(option[$countrySelector.attr('data-valueKey')])
		    	        .text(option[$countrySelector.attr('data-displayKey')]);
		    	      
		    	      $countrySelector.append($option);
	    	    });
    	  });
	}());
	
	// wire cities selector to load up on country selection
	
	var $citySelector = $('#citySelectId');
	
	$countrySelector.change(function loadCities(){
		
		  $.ajax({
    	     	url: $citySelector.attr('data-source'),
    	     	type: "get", //send it through get method
    	     	data: { 
    	     	    'country_code': $countrySelector.find("option:selected").val()
    	     	}
    	  }).then(function(options) {
    		  
    		    $citySelector.empty();
    		  
	    	    options.map(function(option) {
		    	      var $option = $('<option>');
		    	      
		    	      $option
		    	        .val(option[$citySelector.attr('data-valueKey')])
		    	        .text(option[$citySelector.attr('data-displayKey')]);
		    	      
		    	      $citySelector.append($option);
	    	    });
    	  });
	}); 

});