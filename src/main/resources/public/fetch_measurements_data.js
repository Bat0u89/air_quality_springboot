function fetchMeasurementsData(countryCode, city) {

    var $measurementsForm = $('#fetch_measurements_form');
  
    $.ajax({

        url: $measurementsForm.attr('data-source'),
        type: "get",
        dataType: 'json',
        data: { 
            'country_code': countryCode,
            'city': city
        },
        success: function(result) {

            createMeasurementsTableFromJson(result);
        }
    });
};

$(document).ready(function(){
    $("#fetch_measurements_form").submit(function(e) {

        e.preventDefault();
        e.returnValue = false;

        fetchMeasurementsData($('#countrySelectId').find("option:selected").val(), $('#citySelectId').find("option:selected").val());
    });
});