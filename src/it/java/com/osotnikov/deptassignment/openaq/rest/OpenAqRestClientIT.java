package com.osotnikov.deptassignment.openaq.rest;

import java.util.Arrays;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.osotnikov.deptassignment.openaq.rest.model.CityRequest;
import com.osotnikov.deptassignment.openaq.rest.model.CityResponse;
import com.osotnikov.deptassignment.openaq.rest.model.CountryRequest;
import com.osotnikov.deptassignment.openaq.rest.model.CountryResponse;
import com.osotnikov.deptassignment.openaq.rest.model.MeasurementRequest;
import com.osotnikov.deptassignment.openaq.rest.model.MeasurementResponse;
import com.osotnikov.deptassignment.openaq.rest.model.OpenAqModelMetaInfo;

import lombok.extern.slf4j.Slf4j;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Slf4j
class OpenAqRestClientIT {
	
	private static final String TEST_COUNTRY = "AR";
	private static final String TEST_CITY = "Buenos Aires";

	@Autowired
	private OpenAqRestClient openAqRestClient;

	@Test
	public void testGetListOfCountries() {

		CountryRequest countryRequest = CountryRequest.builder()
				.orderBy("desc")
				.limit(5)
				.page(1).build();

		CountryResponse countryResponse = openAqRestClient.getCountries(countryRequest);

		OpenAqModelMetaInfo meta = countryResponse.getMeta();
		Assertions.assertEquals(1, meta.getPage());
		Assertions.assertEquals(5, meta.getLimit());

		Assertions.assertEquals(5, countryResponse.getResults().length);

		Arrays.asList(countryResponse.getResults()).stream().forEach(country -> {

			log.trace("\n\ncountry: {}\n\n", new ReflectionToStringBuilder(country, new RecursiveToStringStyle()).toString());
		});
	}

	@Test
	public void testGetListOfCities() {

		CityRequest cityRequest = CityRequest.builder()
				.orderBy("desc")
				.limit(5)
				.country("US")
				.page(1).build();

		CityResponse cityResponse = openAqRestClient.getCities(cityRequest);

		OpenAqModelMetaInfo meta = cityResponse.getMeta();
		Assertions.assertEquals(1, meta.getPage());
		Assertions.assertEquals(5, meta.getLimit());

		Assertions.assertEquals(5, cityResponse.getResults().length);

		Arrays.asList(cityResponse.getResults()).stream().forEach(city -> {

			Assertions.assertEquals("US", city.getCountry());
			log.trace("\n\ncity: {}\n\n", new ReflectionToStringBuilder(city, new RecursiveToStringStyle()).toString());
		});
	}
	
	@Test
	public void testGetMeasurements() {
		
		MeasurementRequest measReq = MeasurementRequest.builder()
			.country(TEST_COUNTRY)
			.city(TEST_CITY)
			.build();
		
		MeasurementResponse measurementResponse = openAqRestClient.getMeasurements(measReq);
		
		OpenAqModelMetaInfo meta = measurementResponse.getMeta();
		Assertions.assertEquals(1, meta.getPage());
		Assertions.assertTrue(0 < meta.getLimit());

		Assertions.assertTrue(0 < measurementResponse.getResults().size());

		measurementResponse.getResults().stream().forEach(measurementGroup -> {

			log.trace("\n\nmeasurement group: {}\n\n", new ReflectionToStringBuilder(measurementGroup, new RecursiveToStringStyle()).toString());
			
			measurementGroup.getMeasurements().stream().forEach(measurement -> {

				log.trace("\n\nmeasurement: {}\n\n", new ReflectionToStringBuilder(measurement, new RecursiveToStringStyle()).toString());
			});
		});
		
	}

}
