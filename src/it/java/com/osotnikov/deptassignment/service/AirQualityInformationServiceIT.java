package com.osotnikov.deptassignment.service;

import java.util.List;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.osotnikov.deptassignment.model.City;
import com.osotnikov.deptassignment.model.Country;
import com.osotnikov.deptassignment.model.MeasurementGroup;

import lombok.extern.slf4j.Slf4j;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Slf4j
class AirQualityInformationServiceIT {
	
	private static final String TEST_COUNTRY = "AR";
	private static final String TEST_CITY = "Buenos Aires";

	@Autowired
	AirQualityInformationService airQualityInformationService;

	@Test
	void testGetListOfCountries() {

		List<Country> countries = airQualityInformationService.getAllSupportedCountries();

		Assertions.assertTrue(5 < countries.size());

		countries.stream().forEach(country -> {

			log.trace("\n\ncountry: {}\n\n", new ReflectionToStringBuilder(country, new RecursiveToStringStyle()).toString());
		});
	}

	@Test
	void testGetListOfCities() {

		List<City> cities = airQualityInformationService.getCities(
			Country.builder().name("United States").code("US").build()
		);

		Assertions.assertTrue(3 < cities.size());

		cities.stream().forEach(city -> {

			log.trace("\n\ncity: {}\n\n", new ReflectionToStringBuilder(city, new RecursiveToStringStyle()).toString());
		});
	}
	
	@Test
	void testMeasurementGroups() {

		List<MeasurementGroup> measurementGroups = airQualityInformationService.getMeasurementGroups(TEST_COUNTRY, TEST_CITY);

		Assertions.assertTrue(1 < measurementGroups.size());

		measurementGroups.stream().forEach(measurementGroup -> {

			log.trace("\n\nmeasurementGroup: {}\n\n", new ReflectionToStringBuilder(measurementGroup, new RecursiveToStringStyle()).toString());
			
			measurementGroup.getMeasurements().stream().forEach(measurement -> {

				log.trace("\n\nmeasurement: {}\n\n", new ReflectionToStringBuilder(measurement, new RecursiveToStringStyle()).toString());
			});
		});
	}

}
