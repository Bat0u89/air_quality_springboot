package com.osotnikov.web.http;

import java.net.URI;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.osotnikov.deptassignment.openaq.rest.model.CountryRequest;

class HttpUtilsTest {

	private final String BASE_URL = "https://api.openaq.org/v1";
	private static final String URL_PATH = "/countries";
	private final String EXPECTED_URL = "https://api.openaq.org/v1/countries?sort=desc&limit=5&page=1";

	HttpUtilities httpUtils = new HttpUtilities();

	@Test
	void testObjectToQueryString() {

		CountryRequest countryRequest = CountryRequest.builder()
			.limit(5)
			.page(1).sort("desc").build();

		URI uri = httpUtils.createUriWithQueryParams(BASE_URL, URL_PATH, countryRequest);

		Assertions.assertEquals(EXPECTED_URL, uri.toString());
	}

}
